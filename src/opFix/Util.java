/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package opFix;

import java.nio.charset.Charset;
import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import org.postgresql.util.Base64;

/**
 *
 * @author gustavo
 */
public class Util {

    public static String criptografar(String texto) throws Exception {
        KeyGenerator keygenerator = KeyGenerator.getInstance("DES");
        SecretKey myDesKey = keygenerator.generateKey();

        Cipher desCipher;

        // Create the cipher
        desCipher = Cipher.getInstance("DES/ECB/PKCS5Padding");

        // Initialize the cipher for encryption
        desCipher.init(Cipher.ENCRYPT_MODE, myDesKey);

        //sensitive information
        byte[] text = texto.getBytes();

        System.out.println("Text [Byte Format] : " + text);
        System.out.println("Text : " + new String(text));

        // Encrypt the text
        byte[] textEncrypted = desCipher.doFinal(text);

        System.out.println("Text Encryted : " + textEncrypted);
        return new String(textEncrypted);

    }

    public static String desCriptografar(String texto) throws Exception {
        KeyGenerator keygenerator = KeyGenerator.getInstance("DES");
        SecretKey myDesKey = keygenerator.generateKey();

        Cipher desCipher;

        // Create the cipher
        desCipher = Cipher.getInstance("DES/ECB/PKCS5Padding");

        // Initialize the cipher for encryption
        desCipher.init(Cipher.ENCRYPT_MODE, myDesKey);

        //sensitive information
        byte[] text = "No body can see me".getBytes();

        System.out.println("Text [Byte Format] : " + text);
        System.out.println("Text : " + new String(text));

        // Encrypt the text
        byte[] textEncrypted = desCipher.doFinal(text);

        System.out.println("Text Encryted : " + textEncrypted);

        // Initialize the same cipher for decryption
        desCipher.init(Cipher.DECRYPT_MODE, myDesKey);
        textEncrypted = texto.getBytes();
        // Decrypt the text
        byte[] textDecrypted = desCipher.doFinal(textEncrypted);

        System.out.println("Text Decryted : " + new String(textDecrypted));
        return new String(textDecrypted);
    }
    
    public static String base64Encode(String str) {
        String strRet = Base64.encodeBytes(str.getBytes());
        return strRet;
    }
    
    public static String base64Decode(String token) {
        byte[] valueDecoded= Base64.decode(token);
        return new String(valueDecoded);
    }
}
