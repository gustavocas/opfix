/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package opFix;

/**
 *
 * @author gustavo
 */
public class JiraVO {
    public int idOP=0;
    public String descricaoOP="";
    public String jira="";
    public String jiraLogic="";
    public String pone="";
    public String jiraDescricao="";
    public String dataApontamento="";
    public String inicio="";
    public String fim="";
    public String total="";
    public String observacao="";
    
    JiraVO() {

    }

    JiraVO(JiraVO jiraVO) {
        idOP = jiraVO.idOP;
        descricaoOP = jiraVO.descricaoOP;
        jira = jiraVO.jira;
        jiraLogic = jiraVO.jiraLogic;
        pone = jiraVO.pone;
        jiraDescricao = jiraVO.jiraDescricao;
        dataApontamento = jiraVO.dataApontamento;
        inicio = jiraVO.inicio;
        fim = jiraVO.fim;
        total = jiraVO.total;
        observacao = jiraVO.observacao;
    }
    
}
