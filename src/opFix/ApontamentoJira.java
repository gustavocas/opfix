/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package opFix;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.Vector;

/**
 *
 * @author gustavo
 */
public class ApontamentoJira {

    private final int COL_JIRA = 2;
    private final int COL_PONE = 4;
    private final int COL_DESCRICAO = 5;
    private final int COL_DATA = 7;
    private final int COL_INICIO = 8;
    private final int COL_FIM = 9;
    private final int COL_TOTAL = 10;
    private final int COL_OBS = 12;

    public static void main(final String[] args) {
        try {
            String txt = Util.base64Encode("abc123456");
            Util.base64Decode(txt);
        } catch (Exception e) {
            System.out.println("ERRO:" + e.getMessage());
            e.printStackTrace();
        }
    }

    public void obterApontamentosJira() {
        String auxLinha = "";

        try {
            //?os_username=gustavo@logicsp.com.br?os_password=abc123
            //URL url = new URL("http://usi.odebrechtambiental.com/secure/ConfigureReport.jspa?startDate=08%2F02%2F17&finishDate=08%2F02%2F17&projectKey=all&vendorKey=LOGIC&userKey=gcastro&selectedProjectId=11300&reportKey=br.com.mmstech.plugins.atlassian.jira.oa-usi-tools%3AmmstechTimesheetReport&atl_token=BPT7-NKG2-QBCG-181S%7Ca7514dcd8e3b3313abde2feb4f68862575595658%7Clin&Pr%C3%B3ximo=Pr%C3%B3ximo");
            URL url = new URL("http://usi.brkambiental.com.br/");

            URLConnection urlCon = url.openConnection();
            urlCon.setRequestProperty("os_username", "gustavo@logicsp.com.br");
            urlCon.setRequestProperty("os_password", "abc1234");
            urlCon.setRequestProperty("ousername", "gustavo@logicsp.com.br");
            urlCon.setRequestProperty("ologgedInUser", "gustavo@logicsp.com.br");
            urlCon.setRequestProperty("oajs-remote-user", "gustavo@logicsp.com.br");
            urlCon.setRequestProperty("ouserEmail", "gustavo@logicsp.com.br");
            urlCon.setRequestProperty("odata-username", "gustavo@logicsp.com.br");
            urlCon.setRequestProperty("ouser", "gcastro");
            BufferedReader reader = null;
            reader = new BufferedReader(new InputStreamReader(urlCon.getInputStream()));
            while ((auxLinha = reader.readLine()) != null) {
                System.out.println(auxLinha);
            }
        } catch (Exception e) {
            System.out.println("Erro ao ler dados do site:\n" + e.getMessage());
        }
    }

    public Vector<JiraVO> acessaPagina(String usuarioJira, String usuarioJiraEmail, String usuarioJiraSenha, int dia, int mes, int ano) throws Exception {
        BufferedReader rd;
        OutputStreamWriter wr;
        Vector<JiraVO> retorno = new Vector();

        try {
            URL url;
            HttpURLConnection urlConn;
            DataInputStream dis;
            String headerName = null;
            String sessionCookie = null;
            url = new URL("http://usi.brkambiental.com.br/");
            urlConn = (HttpURLConnection) url.openConnection();
            urlConn.setInstanceFollowRedirects(false);
            urlConn.setDoOutput(true);
            String data = URLEncoder.encode("os_username", "UTF-8")
                    + "="
                    + URLEncoder.encode(usuarioJiraEmail, "UTF-8");
            data += "&"
                    + URLEncoder.encode("os_password", "UTF-8")
                    + "="
                    + URLEncoder.encode(usuarioJiraSenha, "UTF-8");
            wr = new OutputStreamWriter(urlConn.getOutputStream());
            wr.write(data);
            wr.flush();
            for (int i = 1; (headerName = urlConn.getHeaderFieldKey(i)) != null; i++) {
                if (headerName.equalsIgnoreCase("Set-Cookie")) {
                    if (sessionCookie == "") {
                        sessionCookie = urlConn.getHeaderField(i);
                    } else {
                        sessionCookie += "; " + urlConn.getHeaderField(i);
                    }
                }
            }
            url = new URL("http://usi.brkambiental.com.br/secure/ConfigureReport.jspa?startDate=" + dia + "%2F" + mes + "%2F" + (ano - 2000) + "&finishDate=" + dia + "%2F" + mes + "%2F" + (ano - 2000) + "&projectKey=all&vendorKey=LOGIC&userKey=" + usuarioJira + "&selectedProjectId=11300&reportKey=br.com.mmstech.plugins.atlassian.jira.oa-usi-tools%3AmmstechTimesheetReport&atl_token=BPT7-NKG2-QBCG-181S%7Ca7514dcd8e3b3313abde2feb4f68862575595658%7Clin&Pr%C3%B3ximo=Pr%C3%B3ximo");
            urlConn = (HttpURLConnection) url.openConnection();
            urlConn.setRequestProperty("Cookie", sessionCookie);
            // Pega a resposta
            rd = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));
            String line;
            int itens = 0;
            JiraVO jira = new JiraVO();
            while ((line = rd.readLine()) != null) {
                if (line.contains("vertical-align")) {
                    if (itens == COL_JIRA) {
                        jira.jira = (String) line.subSequence(line.indexOf(">") + 1, line.indexOf("</td>"));
                    }
                    if (itens == COL_PONE) {
                        jira.pone = (String) line.subSequence(line.indexOf(">") + 1, line.indexOf("</td>"));
                    }
                    if (itens == COL_DESCRICAO) {
                        jira.jiraDescricao = (String) line.subSequence(line.indexOf(">") + 1, line.indexOf("</td>"));
                    }
                    if (itens == COL_DATA) {
                        jira.dataApontamento = (String) line.subSequence(line.indexOf(">") + 1, line.indexOf("</td>"));
                    }
                    if (itens == COL_INICIO) {
                        jira.inicio = (String) line.subSequence(line.indexOf(">") + 1, line.indexOf("</td>"));
                    }
                    if (itens == COL_FIM) {
                        jira.fim = (String) line.subSequence(line.indexOf(">") + 1, line.indexOf("</td>"));
                    }
                    if (itens == COL_TOTAL) {
                        jira.total = (String) line.subSequence(line.indexOf(">") + 1, line.indexOf("</td>"));
                    }
                    if (itens == COL_OBS) {
                        jira.observacao = getObservacao(line, rd);
                    }
                    if (itens == COL_OBS) {
                        retorno.add(jira);
                        itens = -1;
                        jira = new JiraVO();
                    }
                    itens++;
                }
            }
            return retorno;
        } catch (MalformedURLException mue) {
            throw new Exception("Machado Excetipon:\n" + mue.getMessage());
        } catch (IOException ioe) {
            throw new Exception("Machado Excetipon:\n" + ioe.getMessage());
        }

    }

    private String getObservacao(String linha, BufferedReader rd) throws Exception {
        if (linha.indexOf("</td>") != -1) {
            return (String) linha.subSequence(linha.indexOf(">") + 1, linha.indexOf("</td>"));
        }

        String strRetorno = (String) linha.subSequence(linha.indexOf(">") + 1, linha.length() - 1);

        while ((linha = rd.readLine()) != null) {
            strRetorno += "\n" + linha;
            if (linha.contains("</td>")) {
                return strRetorno.replace("</td>", "");
            }
        }
        return linha;
    }
}
