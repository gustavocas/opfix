/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package opFix;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 *
 * @author Gustavo
 */
public class BuscaPreco {

    /**
     * @param args the command line arguments
     */
    private static final int NOME = 0;
    private static final int VALORMINIMO = 1;
    private static final int URL = 2;
    private static final int FILTRO = 3;
    private static final int FILTRO2 = 4;
    private static final DecimalFormat dFormat = new DecimalFormat("#,##0.00");
    private static final SimpleDateFormat fDataHora = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
    public JFrame framePai = null;
    public boolean debug = false;
    public boolean filtraValor = true;
    private Thread thread2 = null;
    private boolean processando = false;

    public static void main(final String[] args) {
        try {
            java.awt.EventQueue.invokeLater(new Runnable() {

                public void run() {
                    new BuscaPreco().processar(args);
                }
            });
        } catch (Exception e) {
            System.out.println("ERRO:" + e.getMessage());
            e.printStackTrace();
        }
    }

    public void processar(final String[] args) {
        try {
            processando = true;
            if (thread2 == null) {
                thread2 = new Thread(new Runnable() {

                    public void run() {
                        try {

                            for (String arg : args) {
                                if (arg.contains("-d")) {
                                    debug = true;
                                }
                                if (arg.contains("-valor")) {
                                    filtraValor = false;
                                }
                            }
                            while (processando) {
                                String msg = gerarArquivos(debug, filtraValor);
                                if (!msg.equals("")) {
                                    JOptionPane.showMessageDialog(framePai, "Precos Encontrados:" + msg, "Precos Baixos", JOptionPane.INFORMATION_MESSAGE);
                                }
                                System.out.println("Hora gerada: " + fDataHora.format(new Date()));
                                Thread.sleep(1200000);
                            }
                        } catch (Exception ex) {
                            JOptionPane.showMessageDialog(framePai, ex.getMessage(), "ERRO", JOptionPane.ERROR_MESSAGE);
                            ex.printStackTrace();
                        }finally {
                            processando=false;
                            thread2=null;
                        }
                    }
                });
            }else{
                thread2.interrupt();
            }
            thread2.start();
            
        } catch (Exception e) {
            JOptionPane.showMessageDialog(framePai, e.getMessage(), "ERRO", JOptionPane.ERROR_MESSAGE);
            e.printStackTrace();
        }
    }

    private static String gerarArquivos(boolean debug, boolean filtraValor) throws Exception {
        File fSites = new File("sites.txt");
        FileReader fr = null;
        BufferedReader br = null;
        String auxLinha = "";
        String precoBaixo = "";

        fr = new FileReader(fSites);
        br = new BufferedReader(fr);
        while (((auxLinha = br.readLine()) != null)) {
            ArquivoVO a = new ArquivoVO();
            String[] linhaSplit = auxLinha.split("!");

            a.nome = linhaSplit[NOME];
            a.valorMinimo = dFormat.parse(linhaSplit[VALORMINIMO]).doubleValue();
            a.url = linhaSplit[URL];
            a.htmlFiltro = linhaSplit[FILTRO];
            if (linhaSplit.length > FILTRO2) {
                a.htmlFiltro2 = linhaSplit[FILTRO2];
            }
            if (a.nome.contains("--")) {
                continue;
            }

            System.out.println(">>>>>>>>>>>>>>");
            System.out.println(">>>>>>>>>>>>>>" + a.nome);

            URL url = new URL(a.url);
            URLConnection urlCon = url.openConnection();
            BufferedReader reader = null;

            try {
                reader = new BufferedReader(new InputStreamReader(urlCon.getInputStream()));
            } catch (Exception e) {
                System.out.println("Erro ao ler link: " + a.url + "\n" + e.getMessage());
                continue;
            }

            while ((auxLinha = reader.readLine()) != null) {
                if (!debug) {
                    String valor = "";
                    if (auxLinha.contains(a.htmlFiltro)) {
                        int i = auxLinha.indexOf(a.htmlFiltro) + a.htmlFiltro.length();
                        while (i < auxLinha.length() && String.valueOf(auxLinha.charAt(i)).matches("[0-9]|[,|\\.]")) {
                            valor += auxLinha.charAt(i);
                            i++;
                        }
                    } else if (!a.htmlFiltro2.equals("") && auxLinha.contains(a.htmlFiltro2)) {
                        int i = auxLinha.indexOf(a.htmlFiltro2) + a.htmlFiltro2.length();
                        while (i < auxLinha.length() && String.valueOf(auxLinha.charAt(i)).matches("[0-9]|[,|\\.]")) {
                            valor += auxLinha.charAt(i);
                            i++;
                        }
                    }
                    if (!valor.equals("")) {
                        if (filtraValor) {
                            if (dFormat.parse(valor).doubleValue() <= a.valorMinimo) {
                                System.out.println(valor);
                                precoBaixo += "\n" + a.nome + ": " + valor;
                            }
                        } else {
                            System.out.println(valor);
                        }
                    }
                } else {
                    System.out.println(auxLinha);
                }
            }
        }
        return precoBaixo;
    }

    public void parar() {
        if (thread2 != null) {
            thread2=null;
            processando=false;
        }
    }

    private static class ArquivoVO {

        public double valorMinimo = 0;
        public String nome = null;
        public String url = null;
        public File arquivo = null;
        public String htmlFiltro = "";
        public String htmlFiltro2 = "";
    }
}
