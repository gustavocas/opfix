/*
 * frmKill.java
 *
 * Created on 6 de Setembro de 2007, 15:33
 */
package opFix;

import intralogic.local.OrdemProducao;
import intralogic.local.OrdemProducaoHome;
import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Hashtable;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.rmi.PortableRemoteObject;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;

/**
 *
 * @author gustavo
 */
public class frmOPFix extends javax.swing.JFrame {

    private final String IP_PORTA_JBOSS = "172.16.50.2:1099";
    private final String M_SERVIDOR_BANCO = "172.16.50.2";
    private String m_senha = "";
    private String m_usuarioJira = "";
    private String m_usuarioJiraEmail = "";
    private String m_usuarioJiraSenha = "";
    private String m_usuarioJiraLogic = "";
    private ItemCombo profissional = null;
    private ItemCombo usuario = null;
    private int idOP;
    private Calendar calendario = null;
    Vector<Vector<String>> vtDestalhesOP = new Vector();
    int COL_ID = 0;
    int COL_INICIO = 1;
    int COL_TERMINO = 2;
    int COL_TIPO = 3;
    int COL_BOTAO = 4;
    int COL_OBSERVACAO = 5;

    SimpleDateFormat fDataGUI = new SimpleDateFormat("dd/MM/yyyy");
    SimpleDateFormat fDataBD = new SimpleDateFormat("yyyy-MM-dd");
    SimpleDateFormat fDataHoraGUI = new SimpleDateFormat("dd/MM/yyyy HH:mm");
    SimpleDateFormat fDataHoraBD = new SimpleDateFormat("yyyy-MM-dd HH:mm");
    SimpleDateFormat fDataHoraBDSAC = new SimpleDateFormat("yyyyMMdd HH:mm");
    SimpleDateFormat fHora = new SimpleDateFormat("HH:mm");
    SimpleDateFormat fDiaSemana = new SimpleDateFormat("EEEE");
    DecimalFormat fDecimal = new DecimalFormat("00");
    BuscaPreco buscaPreco = null;

    /**
     * Creates new form frmKill
     */
    public frmOPFix(String buscaPreco) {
        try {
            initComponents();
            /*setSize(700, 450);
            setPreferredSize(new Dimension(700, 450));
            setMinimumSize(new Dimension(700, 450));*/
            if (!buscaPreco.equals("bp")) {
                pnlAbas.remove(4);
            }

            lerArquivoConf();

            formataTabela();
            formataTabelaSAC();

            carregarCombos();

            Thread thread2 = new Thread(new Runnable() {

                public void run() {
                    try {
                        while (true) {

                            Thread.sleep(60000);
                            if (fHora.format(new Date()).equals("17:59")) {
                                toFront();
                                JOptionPane.showMessageDialog(null, "Pausar OP!!!", "ATENCAO", JOptionPane.INFORMATION_MESSAGE);
                            }

                        }
                    } catch (InterruptedException ex) {
                        JOptionPane.showMessageDialog(null, ex.getMessage(), "ERRO", JOptionPane.ERROR_MESSAGE);
                    }
                }
            });
            thread2.start();
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(this, ex.getMessage(), "ERRO", JOptionPane.ERROR_MESSAGE);
        }
    }

    private void carregarCombos() throws Exception {
        Connection con = null;
        try {
            String sql = "";

            con = Conecta();
            Statement stmt = con.createStatement();

            sql = "SELECT p.id, p.nome, u.id AS idusuario, u.nome AS usuario,replace(p.email,'@logicsp.com.br','') usuariojiralogic"
                    + " FROM profissional  p"
                    + " JOIN usuario u ON p.idusuario=u.id"
                    + " WHERE u.senha = '" + m_senha + "'";

            ResultSet rs = stmt.executeQuery(sql);
            if (rs.next()) {
                profissional = new ItemCombo(rs.getInt("id"), rs.getString("nome"));
                usuario = new ItemCombo(rs.getInt("idusuario"), rs.getString("usuario"));
                m_usuarioJiraLogic = rs.getString("usuariojiralogic");
            } else {
                if (JOptionPane.showConfirmDialog(this, "Senha invalida. Deseja refazer o arquivo de configuracao?", "ERRO", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE) == JOptionPane.YES_OPTION) {
                    String nomeArq = System.getProperty("user.home") + "//.OPFix.ini";
                    File arq = new File(nomeArq);
                    arq.delete();
                    lerArquivoConf();
                    carregarCombos();
                } else {
                    System.exit(0);
                }
            }

            sql = "SELECT id,substr(assunto,0,40) AS assunto "
                    + " FROM ordemproducao"
                    + " WHERE profissional_id=" + profissional.id
                    + " AND status<>5"
                    + " AND (cancelada=false OR cancelada is null)";
            rs = stmt.executeQuery(sql);
            cmbOP.removeAllItems();
            while (rs.next()) {
                cmbOP.addItem(new ItemCombo(rs.getInt("id"), rs.getInt("id") + "-" + rs.getString("assunto")));
            }

            sql = "SELECT id,substr(titulodefeito,0,30) AS defeito "
                    + " FROM defeito"
                    + " WHERE idprofissionalsolicitado=" + profissional.id
                    + " AND idstatus<>6";
            rs = stmt.executeQuery(sql);
            cmbSAC.removeAllItems();
            while (rs.next()) {
                cmbSAC.addItem(new ItemCombo(rs.getInt("id"), rs.getInt("id") + "-" + rs.getString("defeito")));
            }

            lbStatus.setText("Profissional: " + profissional.descricao);
            if (!m_usuarioJira.equals("")) {
                lbStatus.setText(lbStatus.getText() + " | Jira: " + m_usuarioJira);
            }

        } catch (Exception ex) {
            JOptionPane.showMessageDialog(this, ex.getMessage(), "ERRO", JOptionPane.ERROR_MESSAGE);
        } finally {
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException ex) {
                    JOptionPane.showMessageDialog(this, ex.getMessage(), "ERRO", JOptionPane.ERROR_MESSAGE);
                }
            }
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        bindingGroup = new org.jdesktop.beansbinding.BindingGroup();

        jPopupMenu1 = new javax.swing.JPopupMenu();
        jMenuIDetalhe = new javax.swing.JMenuItem();
        jMenuIVisualizar = new javax.swing.JMenuItem();
        jPopupJira = new javax.swing.JPopupMenu();
        jMenuRemover = new javax.swing.JMenuItem();
        jPopupJiraLogic = new javax.swing.JPopupMenu();
        jMenuItemRemover = new javax.swing.JMenuItem();
        pnlAbas = new javax.swing.JTabbedPane();
        pnlOP = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        txtOP = new javax.swing.JTextField();
        cmbOP = new javax.swing.JComboBox();
        btBuscar1 = new javax.swing.JButton();
        btBuscar = new javax.swing.JButton();
        btSalvar = new javax.swing.JButton();
        btSair = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tbHorarios = new javax.swing.JTable();
        pnlDias = new javax.swing.JPanel();
        pnlData = new javax.swing.JPanel();
        btTraz = new javax.swing.JButton();
        lbData = new javax.swing.JLabel();
        btFrente = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        tbSemana = new javax.swing.JTable();
        pnlSAC = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        txtSAC = new javax.swing.JTextField();
        cmbSAC = new javax.swing.JComboBox();
        btBuscarSAC = new javax.swing.JButton();
        btSalvarSAC = new javax.swing.JButton();
        btSair1 = new javax.swing.JButton();
        jScrollPane3 = new javax.swing.JScrollPane();
        tbSAC = new javax.swing.JTable();
        pnlJira = new javax.swing.JPanel();
        jScrollPane4 = new javax.swing.JScrollPane();
        tbJira = new javax.swing.JTable();
        jPanel3 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txtDataApontamentoJira = new javax.swing.JFormattedTextField();
        btJira = new javax.swing.JButton();
        btGerarApontamentosOP = new javax.swing.JButton();
        pnlBuscaPreco = new javax.swing.JPanel();
        jToggleButtonBuscaPreco = new javax.swing.JToggleButton();
        jCheckFiltrar = new javax.swing.JCheckBox();
        jCheckDebug = new javax.swing.JCheckBox();
        pnlJiraLogic = new javax.swing.JPanel();
        jScrollPane5 = new javax.swing.JScrollPane();
        tbJira1 = new javax.swing.JTable();
        jPanel4 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        txtDataApontamentoJiraLogic = new javax.swing.JFormattedTextField();
        btJiraLogic = new javax.swing.JButton();
        btGerarApontamentosOP1 = new javax.swing.JButton();
        lbStatus = new javax.swing.JLabel();

        jMenuIDetalhe.setText("Detalhes");
        jMenuIDetalhe.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuIDetalheActionPerformed(evt);
            }
        });
        jPopupMenu1.add(jMenuIDetalhe);

        jMenuIVisualizar.setText("Visualizar OP");
        jMenuIVisualizar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuIVisualizarActionPerformed(evt);
            }
        });
        jPopupMenu1.add(jMenuIVisualizar);

        jMenuRemover.setText("Remover Item");
        jMenuRemover.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuRemoverActionPerformed(evt);
            }
        });
        jPopupJira.add(jMenuRemover);

        jMenuItemRemover.setText("Remover");
        jMenuItemRemover.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemRemoverActionPerformed(evt);
            }
        });
        jPopupJiraLogic.add(jMenuItemRemover);

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("OP Fix 1.3.6b");
        setMinimumSize(new java.awt.Dimension(750, 501));
        setPreferredSize(new java.awt.Dimension(750, 465));
        getContentPane().setLayout(new javax.swing.BoxLayout(getContentPane(), javax.swing.BoxLayout.Y_AXIS));

        pnlAbas.setMinimumSize(new java.awt.Dimension(673, 400));
        pnlAbas.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                pnlAbasStateChanged(evt);
            }
        });

        pnlOP.setLayout(new java.awt.BorderLayout());

        jPanel1.setLayout(new javax.swing.BoxLayout(jPanel1, javax.swing.BoxLayout.LINE_AXIS));

        txtOP.setMaximumSize(new java.awt.Dimension(75, 2147483647));
        txtOP.setMinimumSize(new java.awt.Dimension(75, 19));
        txtOP.setPreferredSize(new java.awt.Dimension(75, 19));
        txtOP.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtOPKeyPressed(evt);
            }
        });
        jPanel1.add(txtOP);

        cmbOP.setMaximumSize(new java.awt.Dimension(350, 32767));
        cmbOP.setMinimumSize(new java.awt.Dimension(350, 24));
        cmbOP.setPreferredSize(new java.awt.Dimension(350, 24));
        cmbOP.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cmbOPItemStateChanged(evt);
            }
        });
        jPanel1.add(cmbOP);

        btBuscar1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/opFix/img/refresh.gif"))); // NOI18N
        btBuscar1.setMaximumSize(new java.awt.Dimension(18, 18));
        btBuscar1.setMinimumSize(new java.awt.Dimension(0, 0));
        btBuscar1.setPreferredSize(new java.awt.Dimension(25, 25));
        btBuscar1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btBuscar1ActionPerformed(evt);
            }
        });
        jPanel1.add(btBuscar1);

        btBuscar.setText(" Buscar ");
        btBuscar.setMaximumSize(new java.awt.Dimension(95, 18));
        btBuscar.setMinimumSize(new java.awt.Dimension(85, 18));
        btBuscar.setPreferredSize(new java.awt.Dimension(85, 18));
        btBuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btBuscarActionPerformed(evt);
            }
        });
        jPanel1.add(btBuscar);

        btSalvar.setText("Salvar");
        btSalvar.setMaximumSize(new java.awt.Dimension(79, 18));
        btSalvar.setMinimumSize(new java.awt.Dimension(79, 18));
        btSalvar.setPreferredSize(new java.awt.Dimension(79, 18));
        btSalvar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btSalvarActionPerformed(evt);
            }
        });
        jPanel1.add(btSalvar);

        btSair.setText("Sair");
        btSair.setMaximumSize(new java.awt.Dimension(79, 18));
        btSair.setMinimumSize(new java.awt.Dimension(79, 18));
        btSair.setPreferredSize(new java.awt.Dimension(79, 18));
        btSair.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btSairActionPerformed(evt);
            }
        });
        jPanel1.add(btSair);

        pnlOP.add(jPanel1, java.awt.BorderLayout.PAGE_START);

        jScrollPane1.setMinimumSize(new java.awt.Dimension(600, 450));

        tbHorarios.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
        tbHorarios.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbHorariosMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tbHorarios);

        pnlOP.add(jScrollPane1, java.awt.BorderLayout.CENTER);

        pnlAbas.addTab("OPs", pnlOP);

        pnlDias.setLayout(new java.awt.BorderLayout());

        btTraz.setText("<");
        btTraz.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btTrazActionPerformed(evt);
            }
        });
        pnlData.add(btTraz);

        lbData.setText("Semana: ");
        pnlData.add(lbData);

        btFrente.setText(">");
        btFrente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btFrenteActionPerformed(evt);
            }
        });
        pnlData.add(btFrente);

        pnlDias.add(pnlData, java.awt.BorderLayout.PAGE_START);

        tbSemana.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        tbSemana.setComponentPopupMenu(jPopupMenu1);
        jScrollPane2.setViewportView(tbSemana);

        pnlDias.add(jScrollPane2, java.awt.BorderLayout.CENTER);

        pnlAbas.addTab("Horarios", pnlDias);

        pnlSAC.setLayout(new java.awt.BorderLayout());

        jPanel2.setLayout(new javax.swing.BoxLayout(jPanel2, javax.swing.BoxLayout.LINE_AXIS));

        txtSAC.setMinimumSize(new java.awt.Dimension(150, 19));
        txtSAC.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtSACKeyPressed(evt);
            }
        });
        jPanel2.add(txtSAC);

        cmbSAC.setMaximumSize(new java.awt.Dimension(100, 32767));
        jPanel2.add(cmbSAC);

        btBuscarSAC.setText("Buscar");
        btBuscarSAC.setMaximumSize(new java.awt.Dimension(79, 18));
        btBuscarSAC.setMinimumSize(new java.awt.Dimension(79, 18));
        btBuscarSAC.setPreferredSize(new java.awt.Dimension(79, 18));
        btBuscarSAC.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btBuscarSACActionPerformed(evt);
            }
        });
        jPanel2.add(btBuscarSAC);

        btSalvarSAC.setText("Salvar");
        btSalvarSAC.setMaximumSize(new java.awt.Dimension(79, 18));
        btSalvarSAC.setMinimumSize(new java.awt.Dimension(79, 18));
        btSalvarSAC.setPreferredSize(new java.awt.Dimension(79, 18));
        btSalvarSAC.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btSalvarSACActionPerformed(evt);
            }
        });
        jPanel2.add(btSalvarSAC);

        btSair1.setText("Sair");
        btSair1.setMaximumSize(new java.awt.Dimension(79, 18));
        btSair1.setMinimumSize(new java.awt.Dimension(79, 18));
        btSair1.setPreferredSize(new java.awt.Dimension(79, 18));
        btSair1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btSair1ActionPerformed(evt);
            }
        });
        jPanel2.add(btSair1);

        pnlSAC.add(jPanel2, java.awt.BorderLayout.PAGE_START);

        jScrollPane3.setMinimumSize(new java.awt.Dimension(600, 450));

        tbSAC.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        tbSAC.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
        tbSAC.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbSACMouseClicked(evt);
            }
        });
        jScrollPane3.setViewportView(tbSAC);

        pnlSAC.add(jScrollPane3, java.awt.BorderLayout.CENTER);

        pnlAbas.addTab("SAC", pnlSAC);

        pnlJira.setLayout(new java.awt.BorderLayout());

        tbJira.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "OP", "Jira Logic", "JIRA", "PONE", "Descricao", "Data", "Inicio", "Fim", "Tempo", "Observacao"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                true, true, false, false, false, false, false, false, false, true
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });

        org.jdesktop.beansbinding.Binding binding = org.jdesktop.beansbinding.Bindings.createAutoBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ_WRITE, jPopupJira, org.jdesktop.beansbinding.ObjectProperty.create(), tbJira, org.jdesktop.beansbinding.BeanProperty.create("componentPopupMenu"));
        bindingGroup.addBinding(binding);

        jScrollPane4.setViewportView(tbJira);

        pnlJira.add(jScrollPane4, java.awt.BorderLayout.CENTER);

        jLabel1.setText("Data Apontamentos:");
        jPanel3.add(jLabel1);

        txtDataApontamentoJira.setText(new SimpleDateFormat("dd/MM/yyyy").format(Calendar.getInstance().getTime()));
        jPanel3.add(txtDataApontamentoJira);

        btJira.setText("Pesquisar");
        btJira.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btJiraActionPerformed(evt);
            }
        });
        jPanel3.add(btJira);

        btGerarApontamentosOP.setText("Gerar Apontamentos OP");
        btGerarApontamentosOP.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btGerarApontamentosOPActionPerformed(evt);
            }
        });
        jPanel3.add(btGerarApontamentosOP);

        pnlJira.add(jPanel3, java.awt.BorderLayout.PAGE_END);

        pnlAbas.addTab("Apontamento Jira", pnlJira);

        jToggleButtonBuscaPreco.setText("Buscar Preço");
        jToggleButtonBuscaPreco.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jToggleButtonBuscaPrecoActionPerformed(evt);
            }
        });
        pnlBuscaPreco.add(jToggleButtonBuscaPreco);

        jCheckFiltrar.setSelected(true);
        jCheckFiltrar.setText("Filtrar");
        jCheckFiltrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckFiltrarActionPerformed(evt);
            }
        });
        pnlBuscaPreco.add(jCheckFiltrar);

        jCheckDebug.setText("Debug");
        jCheckDebug.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckDebugActionPerformed(evt);
            }
        });
        pnlBuscaPreco.add(jCheckDebug);

        pnlAbas.addTab("Busca Preço", pnlBuscaPreco);

        pnlJiraLogic.setLayout(new java.awt.BorderLayout());

        tbJira1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "OP", "Jira Logic", "Descricao", "Inicio", "Fim", "Tempo", "Observacao"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                true, false, false, true, true, false, true
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });

        binding = org.jdesktop.beansbinding.Bindings.createAutoBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ_WRITE, jPopupJiraLogic, org.jdesktop.beansbinding.ObjectProperty.create(), tbJira1, org.jdesktop.beansbinding.BeanProperty.create("componentPopupMenu"));
        bindingGroup.addBinding(binding);

        jScrollPane5.setViewportView(tbJira1);

        pnlJiraLogic.add(jScrollPane5, java.awt.BorderLayout.CENTER);

        jLabel2.setText("Data Apontamentos:");
        jPanel4.add(jLabel2);

        txtDataApontamentoJiraLogic.setText(new SimpleDateFormat("dd/MM/yyyy").format(Calendar.getInstance().getTime()));
        txtDataApontamentoJiraLogic.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtDataApontamentoJiraLogicActionPerformed(evt);
            }
        });
        jPanel4.add(txtDataApontamentoJiraLogic);

        btJiraLogic.setText("Pesquisar");
        btJiraLogic.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btJiraLogicActionPerformed(evt);
            }
        });
        jPanel4.add(btJiraLogic);

        btGerarApontamentosOP1.setText("Gerar Apontamentos OP");
        btGerarApontamentosOP1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btGerarApontamentosOP1ActionPerformed(evt);
            }
        });
        jPanel4.add(btGerarApontamentosOP1);

        pnlJiraLogic.add(jPanel4, java.awt.BorderLayout.PAGE_END);

        pnlAbas.addTab("Apontamento Jira Logic", pnlJiraLogic);

        getContentPane().add(pnlAbas);

        lbStatus.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lbStatus.setText("Profissional");
        getContentPane().add(lbStatus);

        bindingGroup.bind();

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private Connection Conecta() throws SQLException {
        org.postgresql.ds.common.BaseDataSource ds = new org.postgresql.ds.common.BaseDataSource() {

            @Override
            public String getDescription() {
                return "Not supported yet.";
            }
        };
        ds.setServerName(M_SERVIDOR_BANCO);
        ds.setPortNumber(5432);
        ds.setDatabaseName("intralogic");
        ds.setUser("postgres");
        ds.setPassword("postgres");

        return ds.getConnection();
    }

    private void btBuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btBuscarActionPerformed
        Connection con = null;
        try {

            setCursor(new Cursor(Cursor.WAIT_CURSOR));

            if (txtOP.getText().equals("")) {
                idOP = ((ItemCombo) cmbOP.getSelectedItem()).id;
            } else {
                try {
                    idOP = Integer.parseInt(txtOP.getText());
                } catch (Exception e) {
                    JOptionPane.showMessageDialog(this, "Digite o numero da OP.", "ERRO", JOptionPane.ERROR_MESSAGE);
                }
            }

            con = Conecta();
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM ordemproducao WHERE id=" + idOP + " and profissional_id=" + profissional.id);
            if (!rs.next()) {
                txtOP.setText("");
                throw new Exception("OP nao encontrada para o profissional " + profissional.descricao);
            }

            rs = stmt.executeQuery("SELECT * FROM op_realizado WHERE op_id=" + idOP + " ORDER by id");

            limpaTabela(tbHorarios);

            DefaultTableModel modelo = (DefaultTableModel) tbHorarios.getModel();

            while (rs.next()) {
                modelo.addRow(new String[]{String.valueOf(rs.getInt("id")),
                    rs.getString("datainicio"),
                    rs.getString("datatermino"),
                    String.valueOf(rs.getInt("tipo")),
                    "",
                    rs.getString("observacao")});
            }

            if (tbHorarios.getRowCount() == 0) {
                if (JOptionPane.showConfirmDialog(this, "OP nao iniciada. Deseja iniciar?", "Iniciar OP", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
                    iniciarOP();
                }
            }
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(this, ex.getMessage(), "ERRO", JOptionPane.ERROR_MESSAGE);
        } finally {
            setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
            try {
                if (con != null) {
                    con.close();
                }
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(this, ex.getMessage(), "ERRO", JOptionPane.ERROR_MESSAGE);
            }
        }
    }//GEN-LAST:event_btBuscarActionPerformed

    private void btSairActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btSairActionPerformed
        System.exit(0);
    }//GEN-LAST:event_btSairActionPerformed

    private void btSalvarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btSalvarActionPerformed
        Connection con = null;
        try {

            validaDados();

            setCursor(new Cursor(Cursor.WAIT_CURSOR));

            con = Conecta();
            con.setAutoCommit(false);
            Statement stmt = con.createStatement();
            String sql = "";
            for (int i = 0; i < tbHorarios.getRowCount(); i++) {
                if (tbHorarios.getValueAt(i, COL_TERMINO) != null) {//se o horario fim nao estiver definido nao aplica validacao
                    validarInterposicao(con, Integer.parseInt(tbHorarios.getValueAt(i, COL_ID).toString()), tbHorarios.getValueAt(i, COL_INICIO).toString(), tbHorarios.getValueAt(i, COL_TERMINO).toString());
                }
                sql = "UPDATE op_realizado SET"
                        + " datainicio = '" + tbHorarios.getValueAt(i, COL_INICIO) + "',";

                if (tbHorarios.getValueAt(i, 2) != null) {
                    sql += " datatermino = '" + tbHorarios.getValueAt(i, COL_TERMINO) + "',";
                }

                sql += " tipo = " + tbHorarios.getValueAt(i, COL_TIPO) + ","
                        + " observacao = '" + tbHorarios.getValueAt(i, COL_OBSERVACAO) + "' "
                        + " WHERE id = " + tbHorarios.getValueAt(i, COL_ID);
                stmt.execute(sql);
            }
            con.commit();
            limpaTabela(tbHorarios);
            txtOP.setText("");
        } catch (Exception ex) {
            try {
                if (con != null) {
                    con.rollback();
                }
                JOptionPane.showMessageDialog(this, ex.getMessage(), "ERRO", JOptionPane.ERROR_MESSAGE);
            } catch (SQLException ex1) {
                JOptionPane.showMessageDialog(this, ex1.getMessage(), "ERRO", JOptionPane.ERROR_MESSAGE);
            }
        } finally {
            setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
            try {
                if (con != null) {
                    con.close();
                }
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(this, ex.getMessage(), "ERRO", JOptionPane.ERROR_MESSAGE);
            }
        }
    }//GEN-LAST:event_btSalvarActionPerformed

    private void txtOPKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtOPKeyPressed
        if (evt.getKeyCode() == 10) {
            btBuscarActionPerformed(null);
        }
    }//GEN-LAST:event_txtOPKeyPressed

    private void tbHorariosMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbHorariosMouseClicked
        if (tbHorarios.getSelectedRow() == tbHorarios.getRowCount() - 1 && tbHorarios.getSelectedColumn() == COL_BOTAO) {
            if (tbHorarios.getRowCount() > 0) {
                if (tbHorarios.getValueAt(tbHorarios.getRowCount() - 1, COL_TERMINO) == null) {
                    pausarOP();
                } else {
                    iniciarOP();
                }
            }
        }
    }//GEN-LAST:event_tbHorariosMouseClicked

    private void pnlAbasStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_pnlAbasStateChanged
        if (pnlAbas.getSelectedIndex() == 1) {
            carregarHorarios();
        }
    }//GEN-LAST:event_pnlAbasStateChanged

    private void btTrazActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btTrazActionPerformed
        calendario.add(Calendar.WEEK_OF_YEAR, -1);
        carregarHorarios();
    }//GEN-LAST:event_btTrazActionPerformed

    private void btFrenteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btFrenteActionPerformed
        calendario.add(Calendar.WEEK_OF_YEAR, 1);
        carregarHorarios();
    }//GEN-LAST:event_btFrenteActionPerformed

    private void txtSACKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtSACKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtSACKeyPressed

    private void btBuscarSACActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btBuscarSACActionPerformed
        Connection con = null;
        try {

            setCursor(new Cursor(Cursor.WAIT_CURSOR));

            int idSAC = 0;
            if (txtSAC.getText().equals("")) {
                idSAC = ((ItemCombo) cmbSAC.getSelectedItem()).id;
            } else {
                try {
                    idSAC = Integer.parseInt(txtSAC.getText());
                } catch (Exception e) {
                    JOptionPane.showMessageDialog(this, "Digite o numero do SAC.", "ERRO", JOptionPane.ERROR_MESSAGE);
                }
            }

            con = Conecta();
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM defeitocomentario WHERE iddefeito=" + idSAC + " ORDER by id");

            limpaTabela(tbSAC);

            DefaultTableModel modelo = (DefaultTableModel) tbSAC.getModel();
            while (rs.next()) {
                modelo.addRow(new String[]{String.valueOf(rs.getInt("id")),
                    String.valueOf(rs.getInt("iddefeito")),
                    fDataHoraGUI.format(fDataHoraBDSAC.parse(rs.getString("datahora"))),
                    String.valueOf(rs.getInt("idusuario")),
                    rs.getString("comentario")});
            }

            if (tbSAC.getRowCount() == 0) {
                JOptionPane.showMessageDialog(this, "Nenhum registro encontrado", "ERRO", JOptionPane.INFORMATION_MESSAGE);
            }
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(this, ex.getMessage(), "ERRO", JOptionPane.ERROR_MESSAGE);
        } finally {
            setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
            try {
                if (con != null) {
                    con.close();
                }
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(this, ex.getMessage(), "ERRO", JOptionPane.ERROR_MESSAGE);
            }
        }
    }//GEN-LAST:event_btBuscarSACActionPerformed

    private void btSalvarSACActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btSalvarSACActionPerformed
        Connection con = null;
        try {

            validaDadosSAC();

            setCursor(new Cursor(Cursor.WAIT_CURSOR));

            con = Conecta();
            con.setAutoCommit(false);
            Statement stmt = con.createStatement();
            String sql = "";
            for (int i = 0; i < tbSAC.getRowCount(); i++) {
                sql = "UPDATE defeitocomentario SET"
                        + " datahora = '" + fDataHoraBDSAC.format(fDataHoraGUI.parse(tbSAC.getValueAt(i, 2).toString())) + "',";

                sql += " idusuario = '" + tbSAC.getValueAt(i, 3) + "',"
                        + " comentario = '" + tbSAC.getValueAt(i, 4) + "' "
                        + " WHERE id = " + tbSAC.getValueAt(i, 0);
                stmt.execute(sql);
            }
            con.commit();
            limpaTabela(tbSAC);
        } catch (Exception ex) {
            try {
                if (con != null) {
                    con.rollback();
                }
                JOptionPane.showMessageDialog(this, ex.getMessage(), "ERRO", JOptionPane.ERROR_MESSAGE);
            } catch (SQLException ex1) {
                JOptionPane.showMessageDialog(this, ex1.getMessage(), "ERRO", JOptionPane.ERROR_MESSAGE);
            }
        } finally {
            setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
            try {
                if (con != null) {
                    con.close();
                }
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(this, ex.getMessage(), "ERRO", JOptionPane.ERROR_MESSAGE);
            }
        }
    }//GEN-LAST:event_btSalvarSACActionPerformed

    private void btSair1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btSair1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btSair1ActionPerformed

    private void tbSACMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbSACMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_tbSACMouseClicked

    private void jMenuIDetalheActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuIDetalheActionPerformed
        if (tbSemana.getSelectedRow() >= 0) {
            if (vtDestalhesOP.size() >= tbSemana.getSelectedColumn()) {
                Vector<String> vtDetalhe = vtDestalhesOP.get(tbSemana.getSelectedColumn());
                if (vtDetalhe.size() > tbSemana.getSelectedRow()) {
                    String msg = vtDetalhe.get(tbSemana.getSelectedRow());
                    JOptionPane.showMessageDialog(this, msg, "Detalhe da OP", JOptionPane.INFORMATION_MESSAGE);
                } else {
                    JOptionPane.showMessageDialog(this, "Nao ha dados para exibicao", "ERRO", JOptionPane.WARNING_MESSAGE);
                }
            } else {
                JOptionPane.showMessageDialog(this, "Nao ha dados para exibicao", "ERRO", JOptionPane.WARNING_MESSAGE);
            }

        } else {
            JOptionPane.showMessageDialog(this, "Selecione um item da tabela", "ERRO", JOptionPane.WARNING_MESSAGE);
        }
    }//GEN-LAST:event_jMenuIDetalheActionPerformed

    private void btBuscar1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btBuscar1ActionPerformed
        try {
            carregarCombos();
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(this, ex.getMessage(), "ERRO", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btBuscar1ActionPerformed

    private void jToggleButtonBuscaPrecoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jToggleButtonBuscaPrecoActionPerformed
        if (jToggleButtonBuscaPreco.isSelected()) {
            if (buscaPreco == null) {
                buscaPreco = new BuscaPreco();
                buscaPreco.framePai = this;
            }
            buscaPreco.processar(new String[0]);
        } else {
            if (buscaPreco != null) {
                buscaPreco.parar();
            }
        }
    }//GEN-LAST:event_jToggleButtonBuscaPrecoActionPerformed

    private void jCheckFiltrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckFiltrarActionPerformed
        if (buscaPreco == null) {
            buscaPreco = new BuscaPreco();
            buscaPreco.framePai = this;
        }
        buscaPreco.filtraValor = jCheckFiltrar.isSelected();
    }//GEN-LAST:event_jCheckFiltrarActionPerformed

    private void jCheckDebugActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckDebugActionPerformed
        if (buscaPreco == null) {
            buscaPreco = new BuscaPreco();
            buscaPreco.framePai = this;
        }
        buscaPreco.debug = jCheckDebug.isSelected();
    }//GEN-LAST:event_jCheckDebugActionPerformed

    private void jMenuIVisualizarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuIVisualizarActionPerformed
        if (tbSemana.getSelectedRow() >= 0) {
            if (vtDestalhesOP.size() >= tbSemana.getSelectedColumn()) {
                Vector<String> vtDetalhe = vtDestalhesOP.get(tbSemana.getSelectedColumn());
                if (vtDetalhe.size() > tbSemana.getSelectedRow()) {
                    String msg = vtDetalhe.get(tbSemana.getSelectedRow());
                    String[] msgAr = msg.split("\n");
                    int op = Integer.parseInt(msgAr[0].split(":")[1].trim());
                    txtOP.setText(msgAr[0].split(":")[1].trim());
                    btBuscarActionPerformed(null);
                    pnlAbas.setSelectedIndex(0);
                } else {
                    JOptionPane.showMessageDialog(this, "Nao ha dados para exibicao", "ERRO", JOptionPane.WARNING_MESSAGE);
                }
            } else {
                JOptionPane.showMessageDialog(this, "Nao ha dados para exibicao", "ERRO", JOptionPane.WARNING_MESSAGE);
            }

        } else {
            JOptionPane.showMessageDialog(this, "Selecione um item da tabela", "ERRO", JOptionPane.WARNING_MESSAGE);
        }
    }//GEN-LAST:event_jMenuIVisualizarActionPerformed

    private void btJiraActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btJiraActionPerformed
        try {
            setCursor(new Cursor(Cursor.WAIT_CURSOR));
            if (m_usuarioJira.equals("")) {
                throw new Exception("Usuario do jira deve ser fornecido por parametro.");
            }
            SimpleDateFormat fData = new SimpleDateFormat("dd/MM/yyyy");
            Date dataApontamento = fData.parse(txtDataApontamentoJira.getText());
            if (!fData.format(dataApontamento).equals(txtDataApontamentoJira.getText())) {
                throw new Exception("Data do apontamento invalida.");
            }
            Calendar cal = Calendar.getInstance();
            cal.setTime(dataApontamento);
            limpaTabela(tbJira);
            ApontamentoJira apontamento = new ApontamentoJira();
            Vector<JiraVO> apontamentos = apontamento.acessaPagina(m_usuarioJira, m_usuarioJiraEmail, m_usuarioJiraSenha, cal.get(Calendar.DAY_OF_MONTH), cal.get(Calendar.MONTH) + 1, cal.get(Calendar.YEAR));
            getOPJira(apontamentos);

            //ordena apontamentos do Jira pela data de inicio
            Collections.sort(apontamentos, new Comparator<JiraVO>() {
                @Override
                public int compare(JiraVO o1, JiraVO o2) {
                    if (o1.inicio.compareTo(o2.fim) < 0) {
                        return -1;
                    } else {
                        return 1;
                    }
                }
            });

            DefaultTableModel modelo = (DefaultTableModel) tbJira.getModel();
            for (JiraVO jira : apontamentos) {
                modelo.addRow(new String[]{String.valueOf(jira.idOP),
                    jira.jiraLogic,
                    jira.jira,
                    jira.pone,
                    jira.jiraDescricao,
                    jira.dataApontamento,
                    jira.inicio,
                    jira.fim,
                    jira.total,
                    jira.observacao});
            }
        } catch (ParseException pex) {
            JOptionPane.showMessageDialog(this, "Data do apontamento invalida.", "ERRO", JOptionPane.WARNING_MESSAGE);
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(this, ex.getMessage(), "ERRO", JOptionPane.WARNING_MESSAGE);
        } finally {
            setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        }


    }//GEN-LAST:event_btJiraActionPerformed

    private void btGerarApontamentosOPActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btGerarApontamentosOPActionPerformed
        int i = 0;
        Connection con = null;
        SimpleDateFormat fDataJira = new SimpleDateFormat("dd/MMM/yy HH:mm");

        try {
            setCursor(new Cursor(Cursor.WAIT_CURSOR));
            con = Conecta();
            con.setAutoCommit(false);
            Statement stmt = con.createStatement();
            for (i = 0; i < tbJira.getRowCount(); i++) {
                if (Integer.parseInt((String) tbJira.getValueAt(i, 0)) == 0) {
                    throw new Exception("Informe a OP da linha " + (i + 1));
                }
                String data = (String) tbJira.getValueAt(i, 5);
                String horaInicio = (String) tbJira.getValueAt(i, 6);
                String horaFim = (String) tbJira.getValueAt(i, 7);
                horaInicio = fDataHoraBD.format(fDataJira.parse(data + " " + horaInicio));
                horaFim = fDataHoraBD.format(fDataJira.parse(data + " " + horaFim));

                validarInterposicao(con, 0, horaInicio, horaFim);

                String sql = "select op.id,status \n"
                        + "from ordemproducao op\n"
                        + "where op.profissional_id=" + profissional.id + "\n"
                        + "and op.id=" + Integer.parseInt((String) tbJira.getValueAt(i, 0));
                ResultSet rs = stmt.executeQuery(sql);
                if (!rs.next()) {
                    throw new Exception("OP " + tbJira.getValueAt(i, 0)
                            + " nao relacionada com o profissional " + profissional.id);
                }

                //verifica status pendente=0, e inicia a OP
                if (rs.getInt("status") == 0) {
                    sql = "update ordemproducao set status=2 where id=" + rs.getInt("id");
                    stmt.execute(sql);
                }

                sql = "insert into op_realizado (op_id,datainicio,datatermino,tipo,observacao) values ("
                        + tbJira.getValueAt(i, 0) + ","
                        + "'" + horaInicio + "',"
                        + "'" + horaFim + "',"
                        + "1,"
                        + "'" + tbJira.getValueAt(i, 9) + "')";
                stmt.execute(sql);
            }
            con.commit();
        } catch (ParseException pex) {
            if (con != null) {
                try {
                    con.rollback();
                } catch (SQLException ex) {
                    JOptionPane.showMessageDialog(this, ex.getMessage(), "ERRO", JOptionPane.WARNING_MESSAGE);
                }
            }
            JOptionPane.showMessageDialog(this, "Verifique a OP e datas do apontamento da linha " + (i + 1), "ERRO", JOptionPane.WARNING_MESSAGE);
        } catch (Exception ex) {
            if (con != null) {
                try {
                    con.rollback();
                } catch (SQLException exs) {
                    JOptionPane.showMessageDialog(this, exs.getMessage(), "ERRO", JOptionPane.WARNING_MESSAGE);
                }
            }
            JOptionPane.showMessageDialog(this, ex.getMessage(), "ERRO", JOptionPane.WARNING_MESSAGE);
        } finally {
            setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
            if (con != null) {
                try {
                    con.commit();
                    con.close();
                } catch (SQLException ex) {
                    JOptionPane.showMessageDialog(this, ex.getMessage(), "ERRO", JOptionPane.ERROR_MESSAGE);
                }
            }
        }
    }//GEN-LAST:event_btGerarApontamentosOPActionPerformed

    private void jMenuRemoverActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuRemoverActionPerformed
        if (tbJira.getSelectedRow() >= 0) {
            DefaultTableModel modelo = (DefaultTableModel) tbJira.getModel();
            modelo.removeRow(tbJira.getSelectedRow());
        } else {
            JOptionPane.showMessageDialog(this, "Selecione um item da tabela", "ERRO", JOptionPane.WARNING_MESSAGE);
        }
    }//GEN-LAST:event_jMenuRemoverActionPerformed

    private void cmbOPItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cmbOPItemStateChanged
        ItemCombo item = (ItemCombo) evt.getItem();
        txtOP.setText(String.valueOf(item.id));
    }//GEN-LAST:event_cmbOPItemStateChanged

    private void btJiraLogicActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btJiraLogicActionPerformed
        try {
            setCursor(new Cursor(Cursor.WAIT_CURSOR));
            if (m_usuarioJira.equals("")) {
                throw new Exception("Usuario do jira deve ser fornecido por parametro.");
            }
            SimpleDateFormat fData = new SimpleDateFormat("dd/MM/yyyy");
            SimpleDateFormat fDataJira = new SimpleDateFormat("yyyy-MM-dd");
            Date dataApontamento = fData.parse(txtDataApontamentoJiraLogic.getText());
            if (!fData.format(dataApontamento).equals(txtDataApontamentoJiraLogic.getText())) {
                throw new Exception("Data do apontamento invalida.");
            }
            Calendar cal = Calendar.getInstance();
            cal.setTime(dataApontamento);
            limpaTabela(tbJira1);
            ApontamentoJiraLogic apontamento = new ApontamentoJiraLogic();
            Vector<JiraVO> apontamentos = apontamento.obterJiraLogic(m_usuarioJiraLogic, fDataJira.format(dataApontamento));
            getOPJira(apontamentos);

            //ordena apontamentos do Jira pela data de inicio
            Collections.sort(apontamentos, new Comparator<JiraVO>() {
                @Override
                public int compare(JiraVO o1, JiraVO o2) {
                    if (o1.inicio.compareTo(o2.fim) < 0) {
                        return -1;
                    } else {
                        return 1;
                    }
                }
            });

            DefaultTableModel modelo = (DefaultTableModel) tbJira1.getModel();
            for (JiraVO jira : apontamentos) {
                modelo.addRow(new String[]{String.valueOf(jira.idOP),
                    jira.jiraLogic,
                    jira.jiraDescricao,
                    jira.inicio,
                    jira.fim,
                    jira.total,
                    jira.observacao});
            }
        } catch (ParseException pex) {
            JOptionPane.showMessageDialog(this, "Data do apontamento invalida.", "ERRO", JOptionPane.WARNING_MESSAGE);
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(this, ex.getMessage(), "ERRO", JOptionPane.WARNING_MESSAGE);
        } finally {
            setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        }
    }//GEN-LAST:event_btJiraLogicActionPerformed

    private void btGerarApontamentosOP1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btGerarApontamentosOP1ActionPerformed
        int i = 0;
        Connection con = null;
        SimpleDateFormat fDataJira = new SimpleDateFormat("dd/MM/yy HH:mm");

        try {
            setCursor(new Cursor(Cursor.WAIT_CURSOR));
            con = Conecta();
            con.setAutoCommit(false);
            Statement stmt = con.createStatement();
            for (i = 0; i < tbJira1.getRowCount(); i++) {
                if (Integer.parseInt((String) tbJira1.getValueAt(i, 0)) == 0) {
                    throw new Exception("Informe a OP da linha " + (i + 1));
                }

                String horaInicio = (String) tbJira1.getValueAt(i, 3);
                String horaFim = (String) tbJira1.getValueAt(i, 4);
                horaInicio = fDataHoraBD.format(fDataJira.parse(horaInicio));
                horaFim = fDataHoraBD.format(fDataJira.parse(horaFim));

                validarInterposicao(con, 0, horaInicio, horaFim);

                String sql = "select op.id,status \n"
                        + "from ordemproducao op\n"
                        + "where op.profissional_id=" + profissional.id + "\n"
                        + "and op.id=" + Integer.parseInt((String) tbJira1.getValueAt(i, 0));
                ResultSet rs = stmt.executeQuery(sql);
                if (!rs.next()) {
                    throw new Exception("OP " + tbJira1.getValueAt(i, 0)
                            + " nao relacionada com o profissional " + profissional.id);
                }

                //verifica status pendente=0, e inicia a OP
                if (rs.getInt("status") == 0) {
                    sql = "update ordemproducao set status=2 where id=" + rs.getInt("id");
                    stmt.execute(sql);
                }

                sql = "insert into op_realizado (op_id,datainicio,datatermino,tipo,observacao) values ("
                        + tbJira1.getValueAt(i, 0) + ","
                        + "'" + horaInicio + "',"
                        + "'" + horaFim + "',"
                        + "1,"
                        + "'" + tbJira1.getValueAt(i, 6) + "')";
                stmt.execute(sql);
            }
            con.commit();
        } catch (ParseException pex) {
            if (con != null) {
                try {
                    con.rollback();
                } catch (SQLException ex) {
                    JOptionPane.showMessageDialog(this, ex.getMessage(), "ERRO", JOptionPane.WARNING_MESSAGE);
                }
            }
            JOptionPane.showMessageDialog(this, "Verifique a OP e datas do apontamento da linha " + (i + 1), "ERRO", JOptionPane.WARNING_MESSAGE);
        } catch (Exception ex) {
            if (con != null) {
                try {
                    con.rollback();
                } catch (SQLException exs) {
                    JOptionPane.showMessageDialog(this, exs.getMessage(), "ERRO", JOptionPane.WARNING_MESSAGE);
                }
            }
            JOptionPane.showMessageDialog(this, ex.getMessage(), "ERRO", JOptionPane.WARNING_MESSAGE);
        } finally {
            setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
            if (con != null) {
                try {
                    con.commit();
                    con.close();
                } catch (SQLException ex) {
                    JOptionPane.showMessageDialog(this, ex.getMessage(), "ERRO", JOptionPane.ERROR_MESSAGE);
                }
            }
        }
    }//GEN-LAST:event_btGerarApontamentosOP1ActionPerformed

    private void txtDataApontamentoJiraLogicActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtDataApontamentoJiraLogicActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtDataApontamentoJiraLogicActionPerformed

    private void jMenuItemRemoverActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemRemoverActionPerformed
        if (tbJira1.getSelectedRow() >= 0) {
            DefaultTableModel modelo = (DefaultTableModel) tbJira1.getModel();
            modelo.removeRow(tbJira1.getSelectedRow());
        } else {
            JOptionPane.showMessageDialog(this, "Selecione um item da tabela", "ERRO", JOptionPane.WARNING_MESSAGE);
        }
    }//GEN-LAST:event_jMenuItemRemoverActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        String paramBuscaPrecot = "";
        if (args.length > 0) {
            paramBuscaPrecot = args[0];
        }
        final String paramBuscaPreco = paramBuscaPrecot;
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new frmOPFix(paramBuscaPreco).setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btBuscar;
    private javax.swing.JButton btBuscar1;
    private javax.swing.JButton btBuscarSAC;
    private javax.swing.JButton btFrente;
    private javax.swing.JButton btGerarApontamentosOP;
    private javax.swing.JButton btGerarApontamentosOP1;
    private javax.swing.JButton btJira;
    private javax.swing.JButton btJiraLogic;
    private javax.swing.JButton btSair;
    private javax.swing.JButton btSair1;
    private javax.swing.JButton btSalvar;
    private javax.swing.JButton btSalvarSAC;
    private javax.swing.JButton btTraz;
    private javax.swing.JComboBox cmbOP;
    private javax.swing.JComboBox cmbSAC;
    private javax.swing.JCheckBox jCheckDebug;
    private javax.swing.JCheckBox jCheckFiltrar;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JMenuItem jMenuIDetalhe;
    private javax.swing.JMenuItem jMenuIVisualizar;
    private javax.swing.JMenuItem jMenuItemRemover;
    private javax.swing.JMenuItem jMenuRemover;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPopupMenu jPopupJira;
    private javax.swing.JPopupMenu jPopupJiraLogic;
    private javax.swing.JPopupMenu jPopupMenu1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JToggleButton jToggleButtonBuscaPreco;
    private javax.swing.JLabel lbData;
    private javax.swing.JLabel lbStatus;
    private javax.swing.JTabbedPane pnlAbas;
    private javax.swing.JPanel pnlBuscaPreco;
    private javax.swing.JPanel pnlData;
    private javax.swing.JPanel pnlDias;
    private javax.swing.JPanel pnlJira;
    private javax.swing.JPanel pnlJiraLogic;
    private javax.swing.JPanel pnlOP;
    private javax.swing.JPanel pnlSAC;
    private javax.swing.JTable tbHorarios;
    private javax.swing.JTable tbJira;
    private javax.swing.JTable tbJira1;
    private javax.swing.JTable tbSAC;
    private javax.swing.JTable tbSemana;
    private javax.swing.JFormattedTextField txtDataApontamentoJira;
    private javax.swing.JFormattedTextField txtDataApontamentoJiraLogic;
    private javax.swing.JTextField txtOP;
    private javax.swing.JTextField txtSAC;
    private org.jdesktop.beansbinding.BindingGroup bindingGroup;
    // End of variables declaration//GEN-END:variables
    private void limpaTabela(JTable tabela) throws Exception {
        DefaultTableModel modelo = (DefaultTableModel) tabela.getModel();
        for (int i = tabela.getRowCount() - 1; i >= 0; i--) {
            modelo.removeRow(i);
        }
    }

    private void formataTabela() throws Exception {
        try {

            // titulos das colunas da tabela
            Object[] colunas = {"id", "Inicio", "Termino", "Tipo", "", "Observacoes"};

            // load de dados na tabela
            tbHorarios.setModel(new DefaultTableModel(colunas, 0) {
                public boolean isCellEditable(int row, int col) {
                    if (col == 0) {
                        return false;
                    } else {
                        return true;
                    }
                }
            });

            // -- configuracoes visuais: ------------------------------------- 
            // altura da linha
            tbHorarios.setRowHeight(20);

            // largura de cada coluna
            int[] tamCol = new int[colunas.length];
            tamCol[COL_ID] = 60;
            tamCol[COL_INICIO] = 190;
            tamCol[COL_TERMINO] = 190;
            tamCol[COL_TIPO] = 40;
            tamCol[COL_BOTAO] = 18;
            tamCol[COL_OBSERVACAO] = 190;

            for (int i = 0; i < tbHorarios.getColumnCount(); i++) {
                javax.swing.table.TableColumn column = tbHorarios.getColumnModel().getColumn(i);
                column.setMinWidth(tamCol[i]);
                column.setMaxWidth(tamCol[i]);
                column.setWidth(tamCol[i]);
                column.setPreferredWidth(tamCol[i]);
            }

            // alinhamento das colunas
            Vector vAlinhamento = new Vector(Arrays.asList(
                    SwingConstants.RIGHT,
                    SwingConstants.LEFT));

            // configura o rendererizador da tabela
            TableCellRendererCustomizado renderer = new TableCellRendererCustomizado(true, false);
            tbHorarios.setDefaultRenderer(Class.forName("java.lang.Object"), renderer);

            tbHorarios.setBackground(Color.WHITE);
            this.update(this.getGraphics());
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        }
    }

    private void formataTabelaSAC() throws Exception {
        try {

            // titulos das colunas da tabela
            Object[] colunas = {"id", "SAC", "Data", "Usuario", "Comentario"};

            // load de dados na tabela
            tbSAC.setModel(new DefaultTableModel(colunas, 0) {
                public boolean isCellEditable(int row, int col) {
                    if (col == 0 || col == 1) {
                        return false;
                    } else {
                        return true;
                    }
                }
            });

            // -- configuracoes visuais: -------------------------------------
            // altura da linha
            tbSAC.setRowHeight(20);

            // largura de cada coluna
            int[] tamCol = new int[colunas.length];
            tamCol[0] = 60;
            tamCol[1] = 60;
            tamCol[2] = 120;
            tamCol[3] = 60;
            tamCol[4] = 600;

            for (int i = 0; i < tbSAC.getColumnCount(); i++) {
                javax.swing.table.TableColumn column = tbSAC.getColumnModel().getColumn(i);
                column.setMinWidth(tamCol[i]);
                column.setMaxWidth(tamCol[i]);
                column.setWidth(tamCol[i]);
                column.setPreferredWidth(tamCol[i]);
            }

            // alinhamento das colunas
            Vector vAlinhamento = new Vector(Arrays.asList(
                    SwingConstants.RIGHT,
                    SwingConstants.LEFT));

            // configura o rendererizador da tabela
            TableCellRendererCustomizado renderer = new TableCellRendererCustomizado(false, false);
            tbSAC.setDefaultRenderer(Class.forName("java.lang.Object"), renderer);

            tbSAC.setBackground(Color.WHITE);
            this.update(this.getGraphics());
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        }
    }

    private void iniciarOP() {
        Connection con = null;
        try {
            con = Conecta();
            con.setAutoCommit(false);

            setCursor(new Cursor(Cursor.WAIT_CURSOR));

            OrdemProducao opBean = null;
            Hashtable env = new Hashtable();
            env.put(Context.INITIAL_CONTEXT_FACTORY,
                    "org.jnp.interfaces.NamingContextFactory");

            env.put(Context.PROVIDER_URL, IP_PORTA_JBOSS);

            env.put("java.naming.factory.url.pkgs",
                    "org.jboss.naming:org.jnp.interfaces");

            Context initctx = new InitialContext(env);
            Object objref = initctx.lookup("iLogic");
            OrdemProducaoHome home = (OrdemProducaoHome) PortableRemoteObject.narrow(objref, OrdemProducaoHome.class);
            opBean = home.create();

            int idTipo = 0;
            if (tbHorarios.getRowCount() > 0) {
                idTipo = Integer.parseInt(tbHorarios.getValueAt(tbHorarios.getRowCount() - 1, COL_TIPO).toString());
            }

            if (idTipo == 0) {
                String sql = "update ordemproducao set status=2 where id=" + idOP;
                Statement st = con.createStatement();
                st.execute(sql);
                idTipo = 1;
            }

            opBean.setProfissional(usuario.id);
            opBean.op_real_inicia(idOP, idTipo);
            btBuscarActionPerformed(null);
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(this, ex.getMessage(), "ERRO", JOptionPane.ERROR_MESSAGE);
            ex.printStackTrace();
        } finally {
            try {
                con.commit();
                if (con != null) {
                    con.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(this, "Erro ao fechar conexao.");
            }

            setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        }
    }

    private void pausarOP() {
        try {

            setCursor(new Cursor(Cursor.WAIT_CURSOR));

            OrdemProducao opBean = null;
            Hashtable env = new Hashtable();
            env.put(Context.INITIAL_CONTEXT_FACTORY,
                    "org.jnp.interfaces.NamingContextFactory");

            env.put(Context.PROVIDER_URL, IP_PORTA_JBOSS);

            env.put("java.naming.factory.url.pkgs",
                    "org.jboss.naming:org.jnp.interfaces");

            Context initctx = new InitialContext(env);
            Object objref = initctx.lookup("iLogic");
            OrdemProducaoHome home = (OrdemProducaoHome) PortableRemoteObject.narrow(objref, OrdemProducaoHome.class
            );
            opBean = home.create();

            opBean.op_real_parar(idOP, "");
            btBuscarActionPerformed(null);
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(this, ex.getMessage(), "ERRO", JOptionPane.ERROR_MESSAGE);
            ex.printStackTrace();
        } finally {
            setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        }
    }

    private void validaDados() throws Exception {
        SimpleDateFormat dataHora = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat data = new SimpleDateFormat("yyyy-MM-dd");
        Date dIni, dFim = null;
        for (int i = 0; i < tbHorarios.getRowCount(); i++) {
            try {
                dIni = dataHora.parse(tbHorarios.getValueAt(i, COL_INICIO).toString());
                if ((tbHorarios.getValueAt(i, COL_TERMINO) != null) && !tbHorarios.getValueAt(i, COL_TERMINO).toString().equals("")) {
                    dFim = dataHora.parse(tbHorarios.getValueAt(i, COL_TERMINO).toString());
                }

                if (dFim != null && dIni.getTime() > dFim.getTime()) {
                    throw new Exception("Verifique a linha " + (i + 1)
                            + "\nData inicial esta maior que a data final.");
                }

                if (dFim != null && !data.format(dFim).equals(data.format(dIni))) {
                    throw new Exception("Verifique a linha " + (i + 1)
                            + "\nO dia de inicio e fim da OP estao diferentes.");
                }

                Integer.parseInt(tbHorarios.getValueAt(i, COL_TIPO).toString());
                dFim = null;
            } catch (ParseException pex) {
                throw new Exception("Verifique as datas:" + tbHorarios.getValueAt(i, 1).toString()
                        + " e " + tbHorarios.getValueAt(i, 2).toString() + " na linha " + (i + 1));
            } catch (NumberFormatException nex) {
                throw new Exception("Verifique o tipo da linha " + (i + 1));
            }
        }
    }

    private void validaDadosSAC() throws Exception {

        for (int i = 0; i < tbSAC.getRowCount(); i++) {
            try {
                fDataHoraGUI.parse(tbSAC.getValueAt(i, 2).toString());
                if ((tbSAC.getValueAt(i, 3) != null) && tbSAC.getValueAt(i, 3).toString().equals("")) {
                    throw new Exception("Informe o usuario.");
                } else {
                    Integer.parseInt(tbSAC.getValueAt(i, 3).toString());
                }

                if ((tbSAC.getValueAt(i, 4) != null) && tbSAC.getValueAt(i, 4).toString().equals("")) {
                    throw new Exception("Informe o comentario.");
                }
            } catch (ParseException pex) {
                throw new Exception("Verifique as datas:" + tbSAC.getValueAt(i, 1).toString()
                        + " e " + tbSAC.getValueAt(i, 2).toString() + " na linha " + (i + 1));
            } catch (NumberFormatException nex) {
                throw new Exception("Verifique o tipo da linha " + (i + 1));
            }
        }
    }

    private void carregarHorarios() {
        Connection con = null;
        try {

            setCursor(new Cursor(Cursor.WAIT_CURSOR));

            Vector<Date> vtSemana = new Vector();
            if (calendario == null) {
                calendario = Calendar.getInstance();
            }

            Calendar c = (Calendar) calendario.clone();

            while (c.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY) {
                c.add(Calendar.DAY_OF_MONTH, -1);
            }

            vtSemana.add(c.getTime());
            c.add(Calendar.DAY_OF_MONTH, 1);
            while (c.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY) {
                vtSemana.add(c.getTime());
                c.add(Calendar.DAY_OF_MONTH, 1);

            }

            lbData.setText("De: " + fDataGUI.format(vtSemana.firstElement())
                    + " Ate: " + fDataGUI.format(vtSemana.lastElement()));

            String[] nomes = new String[vtSemana.size()];
            Vector<Vector<String>> vtHorarios = new Vector();
            con = Conecta();
            Statement stmt = con.createStatement();
            ResultSet rs = null;
            int i = 0;
            int maior = 0;
            vtDestalhesOP = new Vector();
            for (Date date : vtSemana) {
                nomes[i] = fDiaSemana.format(date);
                Vector<String> vtHorariosDia = new Vector();
                Vector<String> vtDetalheOPDia = new Vector();
                String sql = "SELECT opr.datainicio, opr.datatermino, op.assunto, op.id, op.iddefeito, op.origem"
                        + " FROM ordemproducao op "
                        + " JOIN op_realizado opr ON opr.op_id=op.id"
                        + " WHERE op.profissional_id=" + profissional.id
                        + " AND opr.datainicio:: date = '" + fDataBD.format(date) + "%'"
                        + " ORDER BY opr.datainicio";
                rs = stmt.executeQuery(sql);
                while (rs.next()) {
                    vtHorariosDia.add(fHora.format(new Date(rs.getTimestamp("datainicio").getTime())) + " - "
                            + (rs.getTimestamp("datatermino") == null ? "" : fHora.format(new Date(rs.getTimestamp("datatermino").getTime()))));
                    vtDetalheOPDia.add(gerarDetalheOP(rs.getInt("id"), rs.getString("assunto"), rs.getTimestamp("datainicio").getTime(), rs.getTimestamp("datatermino") == null ? 0 : rs.getTimestamp("datatermino").getTime(), rs.getInt("iddefeito"), rs.getString("origem")));
                }
                vtDestalhesOP.add(vtDetalheOPDia);
                vtHorarios.add(vtHorariosDia);

                if (maior < vtHorariosDia.size()) {
                    maior = vtHorariosDia.size();
                }
                i++;
            }
            String[][] dados = new String[maior + 1][vtSemana.size()];

            int lin = 0, col = 0;
            long somaHorarios = 0l;
            for (Vector<String> vtHorariosDia : vtHorarios) {
                lin = 0;
                somaHorarios = 0l;
                for (String horario : vtHorariosDia) {
                    dados[lin][col] = horario;
                    somaHorarios += somarHorario(horario);
                    lin++;
                }
                dados[maior][col] = fDecimal.format((int) somaHorarios / 3600000l) + ":" + fDecimal.format((int) (somaHorarios % 3600000l / 60000l));
                col++;
            }

            DefaultTableModel model = new DefaultTableModel(dados, nomes);
            tbSemana.setModel(model);
            TableCellRendererCustomizado renderer = new TableCellRendererCustomizado(false, true);
            tbSemana.setDefaultRenderer(Class.forName("java.lang.Object"), renderer);

        } catch (Exception ex) {
            JOptionPane.showMessageDialog(this, ex.getMessage(), "ERRO", JOptionPane.ERROR_MESSAGE);
        } finally {
            setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException ex) {
                    JOptionPane.showMessageDialog(this, ex.getMessage(), "ERRO", JOptionPane.ERROR_MESSAGE);
                }
            }
        }
    }

    private long somarHorario(String horario) {
        String[] horariosAr = horario.split("-");

        if (horariosAr.length != 2) {
            return 0;
        }
        if (horariosAr[0].trim().equals("") || horariosAr[1].trim().equals("")) {
            return 0;
        }

        long inicio = (Integer.parseInt(horariosAr[0].trim().split(":")[0]) * 3600000l)
                + (Integer.parseInt(horariosAr[0].trim().split(":")[1]) * 60000l);

        long fim = (Integer.parseInt(horariosAr[1].trim().split(":")[0]) * 3600000l)
                + (Integer.parseInt(horariosAr[1].trim().split(":")[1]) * 60000l);

        return fim - inicio;

    }

    private String gerarDetalheOP(int idOP, String assunto, long inicio, long termino, int idDefeito, String origem) {
        String msg = "";
        msg += "OP: " + idOP;
        msg += "\nAssunto: " + assunto;
        if (idDefeito > 0) {
            msg += "\nSAC: " + idDefeito;
        }
        msg += "\nInicio: " + fDataHoraGUI.format(new Date(inicio));
        if (termino != 0) {
            msg += "\nFim: " + fDataHoraGUI.format(new Date(termino));
        }
        if (termino != 0) {
            msg += "\nTempo Gasto: " + fDecimal.format((int) (termino - inicio) / 3600000l) + ":" + fDecimal.format((int) ((termino - inicio) % 3600000l / 60000l));
        };
        if (origem != null && !origem.equals("")) {
            msg += "\nOrigem: " + origem;
        }

        return msg;

    }

    private void getOPJira(Vector<JiraVO> apontamentos) {
        Connection con = null;
        try {
            con = Conecta();
            Statement stmt = con.createStatement();

            for (JiraVO jira : apontamentos) {
                String sql = "select max(id) idOP,max(assunto) op from ordemproducao\n"
                        + "where profissional_id=\n" + profissional.id
                        + "and (1=2\n";
                if (!jira.jira.equals("")) {
                    sql += "or upper(assunto) like upper('%" + jira.jira + "%')\n";
                }
                if (!jira.jira.equals("")) {
                    sql += "or upper(jira) like upper('%" + jira.jira + "%')\n";
                }
                if (!jira.jira.equals("")) {
                    sql += "or upper(origem) like upper('%" + jira.jira + "%')\n";
                }
                if (!jira.jira.equals("")) {
                    sql += "or upper(assunto) like upper('%" + jira.pone + "%')\n";
                }
                if (!jira.jira.equals("")) {
                    sql += "or upper(jira) like upper('%" + jira.pone + "%')\n";
                }
                if (!jira.jira.equals("")) {
                    sql += "or upper(origem) like upper('%" + jira.pone + "%')\n";
                }
                if (!jira.jiraLogic.equals("")) {
                    sql += "or upper(assunto) like upper('%" + jira.jiraLogic + "%')\n";
                }
                if (!jira.jiraLogic.equals("")) {
                    sql += "or upper(jira) like upper('%" + jira.jiraLogic + "%')\n";
                }
                if (!jira.jiraLogic.equals("")) {
                    sql += "or upper(origem) like upper('%" + jira.jiraLogic + "%')\n";
                }
                        sql+= ")";
                ResultSet rs = stmt.executeQuery(sql);
                if (rs.next()) {
                    jira.idOP = rs.getInt("idOP");
                    jira.descricaoOP = rs.getString("op");
                }
                
                if (jira.jiraLogic.equals("")) {
                    jira.jiraLogic = (new ApontamentoJiraLogic()).obterJiraLogicPone(m_usuarioJiraLogic, jira.jira);
                }

            }
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(this, ex.getMessage(), "ERRO", JOptionPane.ERROR_MESSAGE);
        } finally {
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException ex) {
                    JOptionPane.showMessageDialog(this, ex.getMessage(), "ERRO", JOptionPane.ERROR_MESSAGE);
                }
            }
        }

    }

    private void lerArquivoConf() throws Exception {
        String nomeArq = System.getProperty("user.home") + "//.OPFix.ini";
        File arqEscrever = new File(nomeArq);
        if (!arqEscrever.exists()) {
            arqEscrever.createNewFile();

            FileWriter arqCriado = new FileWriter(nomeArq);
            PrintWriter gravarArq = new PrintWriter(arqCriado);
            String senhaDigitada = JOptionPane.showInputDialog(this, "Arquivo de configuracao nao localizado."
                    + "\nPara criar configuracao, digite a senha do intralogic", "Configuracao Inicial", JOptionPane.INFORMATION_MESSAGE);
            if (senhaDigitada == null) {
                System.exit(0);
            }
            MessageDigest m = MessageDigest.getInstance("MD5");
            m.update(senhaDigitada.getBytes(), 0, senhaDigitada.length());
            String senhaMD5 = "" + new BigInteger(1, m.digest()).toString(16);
            System.out.println("MD5: " + senhaMD5);
            gravarArq.println(senhaMD5);

            gravarArq.println(JOptionPane.showInputDialog(this, "Digite o usuario do Jira", "Configuracao Inicial", JOptionPane.INFORMATION_MESSAGE));
            gravarArq.println(JOptionPane.showInputDialog(this, "Digite o email do Jira", "Configuracao Inicial", JOptionPane.INFORMATION_MESSAGE));
            gravarArq.println(Util.base64Encode(JOptionPane.showInputDialog(this, "Digite a senha do Jira", "Configuracao Inicial", JOptionPane.INFORMATION_MESSAGE)));
            arqCriado.close();
        }
        FileReader arq = new FileReader(nomeArq);
        BufferedReader lerArq = new BufferedReader(arq);
        String linha = lerArq.readLine(); // lê a primeira linha
        int iLinha = 0;
        while (linha != null) {
            if (iLinha == 0) {
                m_senha = linha.toUpperCase();
            }
            if (iLinha == 1) {
                m_usuarioJira = linha;
            }
            if (iLinha == 2) {
                m_usuarioJiraEmail = linha;
            }
            if (iLinha == 3) {
                m_usuarioJiraSenha = Util.base64Decode(linha);
            }
            linha = lerArq.readLine();
            iLinha++;
        }
    }

    private void validarInterposicao(Connection con, int idOPRealizado, String horaInicio, String horaFim) throws Exception {
        Statement stmt = con.createStatement();
        String sql = "select op.id,op.assunto,r.datainicio,r.datatermino\n"
                + "from ordemproducao op\n"
                + "join op_realizado r on r.op_id=op.id\n"
                + "where op.profissional_id=" + profissional.id + "\n"
                + "and (r.datainicio::timestamp+interval '1 second' between '" + horaInicio + "'::timestamp and '" + horaFim + "'::timestamp\n"
                + "or r.datatermino::timestamp-interval '1 second' between '" + horaInicio + "'::timestamp and '" + horaFim + "'::timestamp\n"
                + "or '" + horaInicio + "'::timestamp+interval '1 second' between r.datainicio::timestamp and r.datatermino::timestamp\n"
                + "or '" + horaFim + "'::timestamp-interval '1 second' between r.datainicio::timestamp and r.datatermino::timestamp)";
        if (idOPRealizado > 0) {
            sql += "\n and r.id<>" + idOPRealizado;
        }
        ResultSet rs = stmt.executeQuery(sql);
        if (rs.next()) {
            throw new Exception("Existe uma interposicao de apontamentos:"
                    + "\nOP:" + rs.getInt("id")
                    + "\n" + rs.getString("assunto")
                    + "\nInicio: " + rs.getString("datainicio")
                    + "\nTermino: " + rs.getString("datatermino")
                    + "\n\nRemova o item da grid pelo menu de contexto ou corriga a OP.");
        }
    }

    class TableCellRendererCustomizado extends DefaultTableCellRenderer {

        public Color backgroundColor1 = Color.WHITE;
        public Color backgroundColor2 = new Color(240, 240, 240);
        public Color backgroundColorSelecionado = new Color(109, 204, 255);
        public Color backgroundTotal = new Color(210, 210, 210);
        public Color backgroundSubTotal = new Color(255, 255, 255);
        public Color backgroundSubGrupo = new Color(122, 138, 153);
        public Color foregroundColor;
        public Color selectionBackgroundColor;
        public Color selectionForegroundColor;
        public boolean botao = false;
        public boolean totalizador = false;

        public TableCellRendererCustomizado(boolean botao, boolean totalizador) {
            this.botao = botao;
            this.totalizador = totalizador;
        }

        public Component getTableCellRendererComponent(JTable table, Object value,
                boolean isSelected, boolean hasFocus, int row, int column) {

            Component cell = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

            if (isSelected) {
                setBackground(backgroundColorSelecionado);
                setForeground(Color.BLACK);
            } else if ((row % 2) == 0) {
                setBackground(backgroundColor1);
                setForeground(Color.BLACK);
            } else {
                setBackground(backgroundColor2);
                setForeground(Color.BLACK);
            }

            if (totalizador && row == table.getRowCount() - 1) {
                setBackground(Color.BLACK);
                setForeground(Color.WHITE);
            }

            if (column == 0) {
                setHorizontalAlignment(SwingConstants.RIGHT);
            } else {
                setHorizontalAlignment(SwingConstants.LEFT);
            }

            if (botao) {
                TableColumn column2 = table.getColumnModel().getColumn(COL_BOTAO);
                column2.setCellRenderer(new Imagem());
            }

            return cell;
        }

        public void setBackgroundColor1(Color i_color) {
            backgroundColor1 = i_color;
        }

        public void setBackgroundColor2(Color i_color) {
            backgroundColor2 = i_color;
        }

        public void setForegroundColor(Color i_color) {
            foregroundColor = i_color;
        }

        public void setSelectionBackgroundColor(Color i_color) {
            selectionBackgroundColor = i_color;
        }

        public void setSelectionForegroundColor(Color i_color) {
            selectionForegroundColor = i_color;
        }

    }

    class Imagem extends JLabel implements TableCellRenderer {

        public Color backgroundColor1 = Color.WHITE;
        public Color backgroundColor2 = new Color(240, 240, 240);

        public Imagem() {
            setOpaque(true);
        }

        public Component getTableCellRendererComponent(JTable table,
                Object value, boolean isSelected, boolean hasFocus, int row,
                int column) {

            Icon imagem = new ImageIcon();
            if (row == table.getRowCount() - 1) {
                if (table.getValueAt(row, COL_TERMINO) == null) {
                    imagem = new ImageIcon(getClass().getResource("img/stop.gif"));
                } else {
                    imagem = new ImageIcon(getClass().getResource("img/play.gif"));
                }

            }

            if (isSelected) {
                setBackground(table.getSelectionBackground());
            } else if ((row % 2) == 0) {
                setBackground(backgroundColor1);
            } else {
                setBackground(backgroundColor2);
            }

            setIcon(imagem);
            setText(value.toString());

            return this;
        }
    }

}
