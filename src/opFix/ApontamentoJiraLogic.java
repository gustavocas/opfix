/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package opFix;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.Date;
import java.util.Map;
import java.util.Vector;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import jdk.nashorn.api.scripting.ScriptObjectMirror;
import jdk.nashorn.internal.runtime.JSONListAdapter;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 *
 * @author gustavo
 */
public class ApontamentoJiraLogic {

    public static void main(final String[] args) {
        try {
            new ApontamentoJiraLogic().obterJiraLogic("gustavo", "2019-08-29");
            
        } catch (Exception e) {
            System.out.println("ERRO:" + e.getMessage());
            e.printStackTrace();
        }
    }

    public Vector<JiraVO> obterJiraLogic(String usuarioJiraLogic, String dataApontamento) throws Exception {
        BufferedReader rd;
        String json;
        Vector<JiraVO> retorno = new Vector();
        Vector<JiraVO> retornoAux = new Vector();
        try {
            URL url;
            HttpURLConnection urlConn;
            
            String sessionCookie = getCookie();
            

            //<editor-fold defaultstate="collapsed" desc="obter jiras do dia"> 
            url = new URL("http://172.16.40.53:8080/sr/jira.issueviews:searchrequest-xml/temp/SearchRequest.xml?jqlQuery=worklogAuthor+=+"+usuarioJiraLogic+"+and+worklogDate+=+"+dataApontamento+"&tempMax=100");
            urlConn = (HttpURLConnection) url.openConnection();
            urlConn.setRequestProperty("Cookie", sessionCookie);
            // Pega a resposta

            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            Document doc = db.parse(urlConn.getInputStream());
            doc.getDocumentElement().normalize();

            NodeList rss = doc.getElementsByTagName("rss");
            Element dado = (Element) rss.item(0);
            Element channel = (Element) dado.getElementsByTagName("channel").item(0);
            NodeList itens = channel.getElementsByTagName("item");

            String jiraLogicAssigned = "";
            for (int j = 0; j < itens.getLength(); j++) {
                Node itenNode = itens.item(j);
                if (itenNode.getNodeType() == Node.ELEMENT_NODE) {
                    JiraVO jiraVO = new JiraVO();
                    Element itenNodeElement = (Element) itenNode;
                    jiraVO.jiraLogic = retornaValorItem(itenNodeElement.getElementsByTagName("key"));
                    jiraVO.jiraDescricao = retornaValorItem(itenNodeElement.getElementsByTagName("summary"));
                    retorno.add(jiraVO);
                }
            }
            //</editor-fold>

            for (JiraVO jiraVO : retorno) {
                //obter apontamentos por jira
                url = new URL("http://172.16.40.53:8080/rest/api/2/issue/" + jiraVO.jiraLogic + "/worklog");
                urlConn = (HttpURLConnection) url.openConnection();
                urlConn.setRequestProperty("Cookie", sessionCookie);

                rd = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));
                json = "";

                if ((json = rd.readLine()) != null) {
                    Map<String, Object> mapper = obterJson(json);

                    JSONListAdapter workLogs = (JSONListAdapter) mapper.get("worklogs");
                    //obtem todoso os worlog do jira
                    for (Object workLogObjetc : workLogs) {
                        ScriptObjectMirror workLog = (ScriptObjectMirror) workLogObjetc;
                        //obtem objeto autor do apontamento
                        ScriptObjectMirror author = (ScriptObjectMirror) workLog.get("author");
                        //filtra o worklog pelo nome do usuario
                        if (author.get("key").equals(usuarioJiraLogic)) {
                            String dataHoraInicio = (String) workLog.get("started");
                            //filtra o apontamento pela data
                            if ((new SimpleDateFormat("yyyy-MM-dd")).format((new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSX")).parse(dataHoraInicio)).equals((new SimpleDateFormat("yyyy-MM-dd")).format((new SimpleDateFormat("yyyy-MM-dd")).parse(dataApontamento)))) {
                                long tempGasto = Integer.parseInt(workLog.get("timeSpentSeconds").toString()) * 1000;
                                String tempoGastro = (String) workLog.get("timeSpent");
                                long inicioMili = (new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSX")).parse(dataHoraInicio).getTime();
                                Date dataHoraFim = new Date((inicioMili + tempGasto));
                                if (jiraVO.inicio.equals("")){
                                    jiraVO.inicio = (new SimpleDateFormat("dd/MM/yy HH:mm")).format(inicioMili);
                                    jiraVO.fim = (new SimpleDateFormat("dd/MM/yy HH:mm")).format(dataHoraFim);
                                    jiraVO.total = tempoGastro;
                                }else{
                                    JiraVO jiraNovo = new JiraVO(jiraVO);
                                    jiraNovo.inicio = (new SimpleDateFormat("dd/MM/yy HH:mm")).format(inicioMili);
                                    jiraNovo.fim = (new SimpleDateFormat("dd/MM/yy HH:mm")).format(dataHoraFim);
                                    jiraNovo.total = tempoGastro;
                                    //adiciona no auxiliar pq nao pode manipular lista q esta sendo lida
                                    retornoAux.add(jiraNovo);
                                }
                            }
                        }
                    }
                }
            }
            
            retorno.addAll(retornoAux);
            
            //remove itens sem apontamento
            for (int i=retorno.size()-1;i>=0;i--) {
                JiraVO jiraVO = retorno.get(i);
                if (jiraVO.inicio.equals("")){
                    retorno.removeElementAt(i);
                }
            }
            
            return retorno;
        } catch (MalformedURLException mue) {
            throw new Exception("Jira Excetipon:\n" + mue.getMessage());
        } catch (IOException ioe) {
            throw new Exception("Jira Excetipon:\n" + ioe.getMessage());
        }
    }
    
    public Map obterJson(String json) throws Exception {
        ScriptEngine engine = new ScriptEngineManager().getEngineByName("javascript");
        Map<String, Object> mapper = (Map<String, Object>) engine.eval("Java.asJSONCompatible(" + json + ")");
        
        return mapper;
    }
    
    private String retornaValorItem(NodeList i_item) throws Exception {
        String sRetorno = "";

        if (i_item.item(0) != null) {
            NodeList itemValor = ((Element) i_item.item(0)).getChildNodes();
            Node node = (Node) itemValor.item(0);
            if (node != null) {
                sRetorno = node.getNodeValue();
            }
        }

        return sRetorno;
    }
    
    public String obterJiraLogicPone(String usuarioJiraLogic, String jira) throws Exception {
        Vector<JiraVO> retorno = new Vector();

        try {
            URL url;
            HttpURLConnection urlConn;
            
            String sessionCookie = getCookie();
            
            url = new URL("http://172.16.40.53:8080/sr/jira.issueviews:searchrequest-xml/temp/SearchRequest.xml?jqlQuery=summary+~+"+jira+"&tempMax=100");
            urlConn = (HttpURLConnection) url.openConnection();
            urlConn.setRequestProperty("Cookie", sessionCookie);
            // Pega a resposta
 

            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            Document doc = db.parse(urlConn.getInputStream());
            doc.getDocumentElement().normalize();

            NodeList rss = doc.getElementsByTagName("rss");
            Element dado = (Element) rss.item(0);
            Element channel = (Element) dado.getElementsByTagName("channel").item(0);
            NodeList itens = channel.getElementsByTagName("item");
            
            String jiraLogicAssigned="";
            String jiraLogicUnAssigned="";
            for (int j = 0; j < itens.getLength(); j++) {
                Node itenNode = itens.item(j);
                if (itenNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element itenNodeElement = (Element) itenNode;
                    if (usuarioJiraLogic.equals(itenNodeElement.getElementsByTagName("assignee").item(0).getAttributes().getNamedItem("username").getNodeValue())){
                        jiraLogicAssigned += jiraLogicAssigned.equals("")?"":",";
                        jiraLogicAssigned += retornaValorItem(itenNodeElement.getElementsByTagName("key"));
                    }else{
                        jiraLogicUnAssigned += jiraLogicUnAssigned.equals("")?"":",";
                        jiraLogicUnAssigned += retornaValorItem(itenNodeElement.getElementsByTagName("key"));
                    }
                }
            }
            return jiraLogicAssigned.equals("")?jiraLogicUnAssigned:jiraLogicAssigned;
        } catch (MalformedURLException mue) {
            throw new Exception("Jira Excetipon:\n" + mue.getMessage());
        } catch (IOException ioe) {
            throw new Exception("Jira Excetipon:\n" + ioe.getMessage());
        }

    }
    
    public void criarApontamento() {
        /*
        Para criar o apontamento no jira logic, chamar a URL abaixo, passando por posto json:
        http://172.16.40.53:8080/rest/api/2/issue/BSCDV-21/worklog
        body: {"comment":"I did some work here.","started":"2019-06-21T00:00:00.000+0000","timeSpentSeconds":12000}
        */
    }

    private String getCookie() throws Exception {
        String usuarioJira = "gustavo";
        String usuarioJiraEmail = "gustavo@logicsp.com.br";
        String usuarioJiraSenha = Util.base64Decode("dmFjYUE2RTlEMg==");
        URL url;
        HttpURLConnection urlConn;
        OutputStreamWriter wr;
        String sessionCookie="";
                
        String headerName = null;
        url = new URL("http://172.16.40.53:8080/");
        urlConn = (HttpURLConnection) url.openConnection();
        urlConn.setInstanceFollowRedirects(false);
        urlConn.setDoOutput(true);
        String data = URLEncoder.encode("os_username", "UTF-8")
                + "="
                + URLEncoder.encode(usuarioJira, "UTF-8");
        data += "&"
                + URLEncoder.encode("os_password", "UTF-8")
                + "="
                + URLEncoder.encode(usuarioJiraSenha, "UTF-8");

        wr = new OutputStreamWriter(urlConn.getOutputStream());
        wr.write(data);
        wr.flush();
        for (int i = 1; (headerName = urlConn.getHeaderFieldKey(i)) != null; i++) {
            if (headerName.equalsIgnoreCase("Set-Cookie")) {
                if (sessionCookie == "") {
                    sessionCookie = urlConn.getHeaderField(i);
                } else {
                    sessionCookie += "; " + urlConn.getHeaderField(i);
                }
            }
        }
        return sessionCookie;
    }

}
